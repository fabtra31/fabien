﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Hotel
    {
        private int Id;
        private string Nom;
        private string Adresse;
        private string Ville;        public Hotel(int unId, string unNom, string uneAdresse, string uneVille)
        {
            Id = unId;
            Nom = unNom;
            Adresse = uneAdresse;
            Ville = uneVille;
        }

        public int Id1 { get => Id; set => Id = value; }
        public string Nom1 { get => Nom; set => Nom = value; }
        public string Adresse1 { get => Adresse; set => Adresse = value; }
        public string Ville1 { get => Ville; set => Ville = value; }
    }
    
}
