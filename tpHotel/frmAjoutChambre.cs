﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutChambre : Form
    {
        public frmAjoutChambre()
        {
            InitializeComponent();
        }

        private void txtDescription_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNumEtage_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btnValider_Click(object sender, EventArgs e)
        {
            foreach (Hotel Item in Persistance.getLesHotels())
            {
                if (Item.Nom1 == txtHotel.Text)
                {
                    int idHotel = Item.Id1;
                    Persistance.ajouteChambre((int)txtNumEtage.Value, txtDescription.Text, idHotel);
                    MessageBox.Show("Ajout", "Les valeurs ont été ajoutées", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            txtDescription.Text = "";
            txtNumEtage.Text = "0";
            txtHotel.Text = "";
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmAjoutChambre_Load(object sender, EventArgs e)
        {

            foreach (Hotel item in Persistance.getLesHotels())
            {
                txtHotel.Items.Add(item.Nom1);
            }
                
        }
    }
}
